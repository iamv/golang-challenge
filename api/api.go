package main

import (
	"encoding/json"
	"fmt"
	"log"
	"net/http"
	"os"
	"time"
)

type Rectangle struct {
	X      int `json:"x"`
	Y      int `json:"y"`
	Width  int `json:"width"`
	Height int `json:"height"`
}

type ServicePayload struct {
	Main   Rectangle   `json:"main"`
	Inputs []Rectangle `json:"inputs"`
}

type SavedRectangle struct {
	X      int       `json:"x"`
	Y      int       `json:"y"`
	Width  int       `json:"width"`
	Height int       `json:"height"`
	Time   time.Time `json:"time"`
}

type ServiceResponse []SavedRectangle

func main() {
	http.HandleFunc("/", handler)
	log.Fatal(http.ListenAndServe("localhost:8080", nil))
}

func handler(w http.ResponseWriter, r *http.Request) {
	switch r.Method {
	case "GET":
		rects := readRectanglesFromFile("out.json")
		json.NewEncoder(w).Encode(rects)
	case "POST":
		var payload ServicePayload
		decoder := json.NewDecoder(r.Body)
		err := decoder.Decode(&payload)
		if err != nil {
			log.Fatal(err)
		}
		out := intersectingRectangles(payload)
		saveRectanglesAppendToTheEndOfFile(out, "out.json")
	default:
		fmt.Fprintf(w, "Blah Blaah")
	}
}

func intersectingRectangles(payload ServicePayload) []Rectangle {
	main := payload.Main
	inputs := payload.Inputs
	var out []Rectangle
	for _, input := range inputs {
		if main.X < input.X+input.Width && main.X+main.Width > input.X && main.Y < input.Y+input.Height && main.Y+main.Height > input.Y {
			out = append(out, input)
		}
	}
	return out
}

func saveRectanglesAppendToTheEndOfFile(rectangles []Rectangle, fileName string) {
	currentTime := time.Now()

	for _, rectangle := range rectangles {
		savedRectangle := SavedRectangle{
			X:      rectangle.X,
			Y:      rectangle.Y,
			Width:  rectangle.Width,
			Height: rectangle.Height,
			Time:   currentTime,
		}
		saveRectangleToFile(savedRectangle, fileName)
	}
}

func saveRectangleToFile(rectangle SavedRectangle, filename string) {
	file, err := os.OpenFile(filename, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	encoder := json.NewEncoder(file)
	err = encoder.Encode(rectangle)
	if err != nil {
		log.Fatal(err)
	}
}

func readRectanglesFromFile(filename string) ServiceResponse {
	file, err := os.Open(filename)
	if err != nil {
		log.Fatal(err)
	}
	defer file.Close()
	var rectangles ServiceResponse
	decoder := json.NewDecoder(file)
	for {
		var rectangle SavedRectangle
		err := decoder.Decode(&rectangle)
		if err != nil {
			break
		}
		rectangles = append(rectangles, rectangle)
	}
	return rectangles
}
